import { PieceTypes, PlayerColor } from "../lib/piece"

const initialBoard = [
  [
    {
      pieceType: PieceTypes.rook,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.knight,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.bishop,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.queen,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.king,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.bishop,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.knight,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.rook,
      initial: true,
      color: PlayerColor.black
    }
  ],
  [
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.black
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.black
    }
  ],
  [
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
  ],
  [
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
  ],
  [
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
  ],
  [
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
  ],
  [
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.pawn,
      initial: true,
      color: PlayerColor.white
    }
  ],
  [
    {
      pieceType: PieceTypes.rook,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.knight,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.bishop,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.queen,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.king,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.bishop,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.knight,
      initial: true,
      color: PlayerColor.white
    },
    {
      pieceType: PieceTypes.rook,
      initial: true,
      color: PlayerColor.white
    }
  ],
]

export default initialBoard