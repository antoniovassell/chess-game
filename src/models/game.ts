import { ObjectId } from 'mongodb'
import { PieceTypes, PlayerColor } from '../lib/piece';

export interface PieceConfig {
  initial: boolean,
  color: PlayerColor,
  pieceType: PieceTypes
}

export default class Game {
  constructor (
    public board: PieceConfig[][],
    public playersTurn?: PlayerColor,
    public _id?: ObjectId
  ) {
    if (!this.playersTurn) {
      this.playersTurn = PlayerColor.white
    }
  }
}