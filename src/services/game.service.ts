import { ObjectId } from 'mongodb'
import { collections } from '../services/database.service'
import Game from '../models/game'
import boardConfig from '../config/initial-board-state'
import Board from '../lib/board'
import Piece, { MoveByString, PieceTypes, PlayerColor, Position } from '../lib/piece'
import Pawn from '../lib/pawn'
import BadRequestHttpException from '../common/bad-request-http-error'
import NotFoundHttpException from '../common/not-found-http-error'
import { PieceFactory } from '../lib/piece-factory'


export default class GameService {
  async create() {
    try {
      const board = new Board(boardConfig)
      const newGame = new Game(board.layout)
      const result = await collections.games.insertOne(newGame)

      const query = { _id: result.insertedId }
      const savedGame = (await collections.games.findOne(query)) as Game
      return savedGame
    } catch (error) {
      throw error
    }
  }

  async get (id: string) {
    return this.getGame(id)
  }

  async move (gameId: string, moveOptions: MoveByString, player: PlayerColor) {
    // dicher position
    const convertedMove = this.dicherChessPosition(moveOptions)

    // initialize board and piece
    const game = await this.getGame(gameId)

    if (game.playersTurn !== player) {
      throw new BadRequestHttpException('InvalidTurn', 'Not your turn')
    }

    const pieceDetail = game.board[convertedMove.from.row][convertedMove.from.col]

    if (!pieceDetail) {
      throw new BadRequestHttpException('InvalidSquare', 'Square does not have a piece')
    }

    if (pieceDetail.pieceType !== PieceTypes.pawn) {
      throw new BadRequestHttpException('InvalidPiece', 'Can only use pawns')
    }

    if (pieceDetail.color !== player) {
      throw new BadRequestHttpException('InvalidPiece', 'Can only move pieces that you own')
    }

    const piece: Piece = PieceFactory.createPiece({
      board: new Board(game.board),
      player,
      position: convertedMove.from,
      initial: pieceDetail.initial,
      pieceType: pieceDetail.pieceType
    })

    piece.move(convertedMove)

    // determine next player
    game.playersTurn = game.playersTurn === PlayerColor.black ? PlayerColor.white : PlayerColor.black
    // save board
    await collections.games.updateOne(
      {_id: game._id},
      {$set: {
        board: game.board,
        playersTurn: game.playersTurn
      }}
    )

    return true
  }

  async getGame(id: string) : Promise<Game> {
    const query = { _id: new ObjectId(id)}
    const game = (await collections.games.findOne(query)) as Game

    if (!game) throw new NotFoundHttpException('GameNotFound', 'Game not found')

    return game
  }

  dicherChessPosition (move: MoveByString) {
    return {
      from: this.convertPositionToNumber(move.from),
      to: this.convertPositionToNumber(move.to)
    }
  }

  async getMoves (gameId: string, square: string)  {
    const game = await this.getGame(gameId)

    const position = this.convertPositionToNumber(square)
    const pieceDetail = game.board[position.row][position.col]

    if (!pieceDetail) {
      throw new BadRequestHttpException('InvalidSquare', 'Square does not have a piece')
    }

    if (pieceDetail.pieceType !== PieceTypes.pawn) {
      throw new BadRequestHttpException('InvalidPiece', 'Can only use pawns')
    }

    const piece: Piece = PieceFactory.createPiece({
      board: new Board(game.board),
      player: pieceDetail.color,
      position,
      initial: pieceDetail.initial,
      pieceType: pieceDetail.pieceType
    })

    const moves = piece.getPotentialMoves()
    const movesToString = []

    for (const move of moves) {
      const positionToString = this.convertPositionToString({row: move.row, col: move.col})
      movesToString.push({
        position: positionToString,
        capture: move.capture
      })
    }

    return movesToString
  }

  convertPositionToNumber (position: string) {
    if (position.length > 2) {
      throw new BadRequestHttpException('InvalidPosition', 'Invalid position')
    }

    let row: number = 0
    let col: number = 0

    col = position.charCodeAt(0) - 'a'.charCodeAt(0)
    row = 8 - parseInt(position.charAt(1), 10)

    if (row < 0 || row > 7 || col < 0 || col > 7) {
      throw new BadRequestHttpException('InvalidPosition', 'Invalid position')
    }

    return {row, col}
  }

  convertPositionToString (position: Position) : string {
    const col: string = String.fromCharCode(97 + position.col)
    const row: number = 8 - position.row

    return `${col}${row}`
  }
}