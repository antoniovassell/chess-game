import express from 'express'
import routes from './routes'

import { errorHandler } from './middlewares/error.middleware'
import { notFoundHandler } from './middlewares/not-found.middleware'

import { connectToDatabase } from './services/database.service'

const port = 9000

const app = express()

app.use(express.json())
connectToDatabase().then(() => {
  app.use(routes)

  app.use(errorHandler)
  app.use(notFoundHandler)
}).catch((error: Error) => {
  console.error('Database connection error: ', error)
})

export default app