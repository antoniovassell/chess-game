import {Router} from 'express'
import gamesRouter from './games'

const routes = Router()

routes.use('/games', gamesRouter)

export default routes