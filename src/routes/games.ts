import { Router, Request, Response } from "express"
import ServerNotImplementedHttpException from "../common/server-http-error"

import GamesController from '../controllers/game.controller'

const gamesRouter = Router()

/**
 * @api {post} /games/:id/move Move piece
 * @apiName MovePiece
 * @apiGroup Game
 *
 * @apiParam {String} id     Unique ID for game, should be hex 24 characters
 *
 * @apiBody {String}  from    Position of the square that the piece is moving from (eg "a2")
 * @apiBody {String}  to      Position of the square that the piece is moving to (eg "a4")
 * @apiBody {String}  player  Player that is making the move
 *
 * @apiError InvalidPiece       Ensure to specify a valid game id
 * @apiError InvalidPosition    Ensure to specify a valid game id
 * @apiError InvalidSquare      Square does not have a piece
 * @apiError GameNotFound       Game not found with that ID
 * @apiError InvalidGameId      Invalid game ID
 * @apiError InvalidMove        Invalid move, you cannot move to that new position
 *
 */
gamesRouter.post('/:id/move', async (req: Request, res: Response) => {
  try {
    await GamesController.move(req)
    res.json()
  } catch (err) {
    req.next(err)
  }
})

/**
 * @api {post} /games/ Create game
 * @apiName CreateGame
 * @apiGroup Game
 *
 * @apiSuccess {Object}     game Game object
 * @apiSuccess {String}     game._id Game ID
 * @apiSuccess {String}     game.playersTurn      Name of the player who has the current turn (eg "black", "white")
 * @apiSuccess {Object[][]} game.board            Board with initial pieces
 * @apiSuccess {Boolean}    game.board.initial    If this is the first position for this piece (or initial position)
 * @apiSuccess {String}     game.board.pieceType  Name of the piece (eg "pawn", "king", etc)
 * @apiSuccess {String}     game.board.color      Name of the color or player (eg "black", "white")
 */
gamesRouter.post('/', async (req: Request, res: Response) => {
  try {
    const result = await GamesController.create(req)
    res.json(result)
  } catch (err) {
    req.next(err)
  }
})

/**
 * @api {get} /games/:id/potential/:square Get potential moves
 * @apiName GetPotentialMoves
 * @apiGroup Game
 *
 * @apiParam {String}       id                  Unique ID for game, should be hex 24 characters
 * @apiParam {String}       square              Square location (eg "a1", "c2", etc)
 *
 * @apiSuccess {Object[]}   positions           List of positions that piece could move to
 * @apiSuccess {Boolean}    positions.capture   If going to this position will capture a piece
 * @apiSuccess {String}     positions.position  Location of new square that the piece could move to
 *
 * @apiError InvalidPiece     Currently limited to pawns
 * @apiError InvalidPosition  Invalid position on board
 * @apiError InvalidSquare    Square does not have a piece to move
 * @apiError InvalidGameId    Invalid game ID
 * @apiError GameNotFound     Game not found with that ID
 *
 */
gamesRouter.get('/:id/potential/:square', async (req: Request, res: Response) => {
  try {
    const result = await GamesController.potentialMoves(req)
    res.json(result)
  } catch (err) {
    req.next(err)
  }
})

/**
 * @api {get} /games/:id/history Get game history
 * @apiName GetGameHistory
 * @apiGroup Game
 *
 * @apiParam {String} id      Unique ID for game, should be hex 24 characters
 *
 * @apiError NotImplemented   Functionality not yet implemented
 */
gamesRouter.get('/:id/history', async (req: Request, res: Response) => {
  try {
    throw new ServerNotImplementedHttpException('NotImplemented', 'Not yet implemented')
  } catch (err) {
    req.next(err)
  }
})

/**
 * @api {get} /games/:id    Get an existing game
 * @apiName GetGame
 * @apiGroup Game
 *
 * @apiParam {String} id     Unique ID for game, should be hex 24 characters
 *
 * @apiSuccess {Object}     game Game object
 * @apiSuccess {String}     game._id Game ID
 * @apiSuccess {String}     game.playersTurn      Name of the player who has the current turn (eg "black", "white")
 * @apiSuccess {Object[][]} game.board            Board with initial pieces
 * @apiSuccess {Boolean}    game.board.initial    If this is the first position for this piece (or initial position)
 * @apiSuccess {String}     game.board.pieceType  Name of the piece (eg "pawn", "king", etc)
 * @apiSuccess {String}     game.board.color      Name of the color or player (eg "black", "white")
 *
 * @apiError InvalidGameId  Invalid game ID
 * @apiError GameNotFound   Game not found with that ID
 */
gamesRouter.get('/:id', async (req: Request, res: Response) => {
  try {
    const result = await GamesController.get(req)
    res.json(result)
  } catch (err) {
    req.next(err)
  }
})

export default gamesRouter