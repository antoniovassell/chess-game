import Board from "./board"
import Piece, { MoveByNumber, MoveByString, PieceTypes, PlayerColor, Position, PositionWithAttack } from "./piece"



export default class Pawn extends Piece {
  constructor (
    public board: Board,
    public player: PlayerColor,
    public position: Position,
    public initial: boolean) {
      super(
        board,
        player,
        position,
        initial,
        PieceTypes.pawn
      )
  }

  getPotentialMoves (): PositionWithAttack[] {
    const potentialMoves: PositionWithAttack[] = []

    let forward = 1
    let left = -1
    let right = 1
    if (this.player === PlayerColor.white) {
      forward = -1
      left = 1
      right = -1
    }

    // check if there is an obstacle in front
    let inFront: PositionWithAttack = {row: this.position.row + forward, col: this.position.col, capture: false}
    if (this.isValidPosition(inFront)) {
      let isOccupied = this.board.getPiece(inFront)

      if (!isOccupied) {
        potentialMoves.push(inFront)

        // If first move check if we can go further
        if (this.initial) {
          inFront = {row: inFront.row + forward, col: this.position.col, capture: false}
          if (this.isValidPosition(inFront)) {
            isOccupied = this.board.getPiece(inFront)
            if (!isOccupied) {
              potentialMoves.push(inFront)
            }
          }
        }
      }
    }

    // check if you can attack on the left
    const leftPosition = {row: this.position.row + forward, col: this.position.col + left, capture: true}
    const leftSquare = this.board.getPiece(leftPosition)
    if (leftSquare && leftSquare.color !== this.player) {
      potentialMoves.push(leftPosition)
    }
    // check if you can attack on the right
    const rightPosition = {row: this.position.row + forward, col: this.position.col + right, capture: true}
    const rightSquare = this.board.getPiece(rightPosition)
    if (rightSquare && rightSquare.color !== this.player) {
      potentialMoves.push(rightPosition)
    }

    return potentialMoves
  }
}