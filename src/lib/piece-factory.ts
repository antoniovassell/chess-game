import BadRequestHttpException from "../common/bad-request-http-error"
import Pawn from "./pawn"
import Piece, { PieceParams, PieceTypes } from "./piece"

export class PieceFactory {
  public static createPiece(pieceParams: PieceParams) : Piece {
    let piece : Piece = null
    switch (pieceParams.pieceType) {
      case PieceTypes.pawn:
        piece = new Pawn(
          pieceParams.board,
          pieceParams.player,
          pieceParams.position,
          pieceParams.initial
        )
        break
      default:
        throw new BadRequestHttpException('InvalidPiece', 'Can only use pawns')
    }

    return piece
  }
}