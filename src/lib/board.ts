import boardConfig from "../config/initial-board-state"
import { PieceConfig } from "../models/game"
import { MoveByNumber, Position } from "./piece"

export default class Board {
  constructor (
    public layout: typeof boardConfig
  ) {}

  getBoard () {
    return this.layout
  }

  movePiece (move: MoveByNumber) {
    this.layout[move.to.row][move.to.col] = this.layout[move.from.row][move.from.col]
    this.layout[move.to.row][move.to.col].initial = false

    this.layout[move.from.row][move.from.col] = null

    return true
  }

  getPiece (position: Position) : PieceConfig {
    return this.layout[position.row][position.col]
  }
}