import Board from './board'
import BadRequestHttpException from '../common/bad-request-http-error'

export enum PlayerColor {
  white = 'white',
  black = 'black'
}

export enum PieceTypes {
  rook = 'rook',
  knight = 'knight',
  bishop = 'bishop',
  queen = 'queen',
  king = 'king',
  pawn = 'pawn'
}

export interface MoveByString {
  from: string,
  to: string
}

export interface MoveByNumber {
  from: Position
  to: Position
}

export interface Position {
  row: number,
  col: number
}

export interface PositionWithAttack extends Position {
  capture: boolean
}

export interface PieceParams {
  board: Board,
  player: PlayerColor,
  position: Position,
  initial: boolean,
  pieceType: PieceTypes
}

export default abstract class Piece {
  constructor (
    public board: Board,
    public player: PlayerColor,
    public position: {row: number, col: number},
    public initial: boolean,
    public pieceType: PieceTypes
  ) {}

  getPosition (): Position {
    return this.position
  }

  isValidPosition (position: Position) : boolean {
    if (position.row < 0 || position.row > 7 || position.col < 0 || position.col > 7) {
      return false
    }
    return true
  }

  getPotentialMoves (): PositionWithAttack[] {
    const potentialMoves: PositionWithAttack[] = []

    return potentialMoves
  }

  isValidMove (position: Position) : boolean {
    // Currently we are generating all the possible positions that this pawn/piece can move to
    // And then we check if the specified position is in that list
    // This is very easy to do for Pawn since moves are very limited
    // However other pieces might have a larger potential number of moves
    // A much better way could have been to generate the potential move for the given position, and only search in that direction
    // rather than generating all potential moves
    // So that could be a future improvement
    const potentialPositions: PositionWithAttack[] = this.getPotentialMoves()

    const mapPotentialPositions = {}

    for (const potentialPosition of potentialPositions) {
      mapPotentialPositions[`${potentialPosition.row}-${potentialPosition.col}`] = true
    }

    if (mapPotentialPositions[`${position.row}-${position.col}`]) return true

    return false
  }

  move (
    move: MoveByNumber
  ) : boolean {
    if (!this.isValidMove(move.to)) {
      throw new BadRequestHttpException('InvalidMove', 'Invalid move')
    }

    // update piece position
    this.position = move.to
    if (this.initial) {
      this.initial = false
    }

    // update board
    this.board.movePiece(move)

    return true
  }
}