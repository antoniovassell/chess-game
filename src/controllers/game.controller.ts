import { ObjectId } from 'bson'
import { Request } from 'express'
import BadRequestHttpException from '../common/bad-request-http-error'
import GameService from '../services/game.service'

export default class GamesController {
  static async create (req: Request) {
    const gameService = new GameService()

    return gameService.create()
  }

  static async get (req: Request) {
    const gameService = new GameService()
    if (!ObjectId.isValid(req.params.id)) {
      throw new BadRequestHttpException('InvalidGameId', 'Invalid game id')
    }
    return gameService.get(req.params.id)
  }

  static async move (req: Request) {
    if (!ObjectId.isValid(req.params.id)) {
      throw new BadRequestHttpException('InvalidGameId', 'Invalid game id')
    }

    const gameService = new GameService()

    const move = {
      from: req.body.from,
      to: req.body.to
    }

    return gameService.move(
      req.params.id,
      move,
      req.body.player
    )
  }

  static async potentialMoves (req: Request) {
    if (!ObjectId.isValid(req.params.id)) {
      throw new BadRequestHttpException('InvalidGameId', 'Invalid game id')
    }
    const gameService = new GameService()

    return gameService.getMoves(
      req.params.id,
      req.params.square,
    )
  }
}