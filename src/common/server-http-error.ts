import HttpException from "./http-error"

export default class ServerNotImplementedHttpException extends HttpException {
  constructor(error: string, message: string) {
    super(
      501,
      message,
      error
    )
  }
}