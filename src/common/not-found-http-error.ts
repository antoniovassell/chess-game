import HttpException from "./http-error"

export default class NotFoundHttpException extends HttpException {
  constructor( error: string, message: string) {
    super(
      404,
      message,
      error
    )
  }
}