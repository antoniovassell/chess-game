import HttpException from "./http-error"

export default class BadRequestHttpException extends HttpException {
  constructor(error: string, message: string) {
    super(
      400,
      message,
      error
    )
  }
}