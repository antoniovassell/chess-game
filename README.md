# Chess Game

## Design
- For this small application, current design pattern I choosed was `MVC+Service Layer`.
- The board is represent as a multi-dimensional array with objects representing each piece and square, see `./src/config/intitial-board-state.ts` to see data structure. This structure is what's being saved in the database.
- The main logic is being able to move a piece, and given each piece have different ways of moving, I created a parent `Piece` with common functionality, and then created a `Pawn` class that extends `Piece`. This allows `Pawn` to implement its own logic of doing movement across the board.
- To add more pieces, you could just create a new class that extends `Piece` and implement its own movement logic, eg a `Bishop` or `Knight`.

## Improvements
Additional improvements that could be made in the future:
- There could be a general movement class that handles the general movement of the different pieces, and a `Piece` would just specify its limitations. This would have to take in consideration edge cases for each types of `Piece`.

## Prerequisites
This project is setup to use docker, to avoid manually installing dependencies. However, you will need the following

- docker
- docker-compose
- Available 9000 port

## Installation
- Clone the project from gitlab
  ```
  git clone https://gitlab.com/antoniovassell/chess-game.git
  cd chess-game
  ```
- At the root of the project, run the following command:
  ```
  docker-compose up
  ```

## API
- Use the following url to perform api request `http://localhost:9000/`

## API Documentation
- API documentation is done using [apiDocs](https://apidocjs.com/), didn't use swagger api in the interest of time, since I haven't used it in a long time and we mainly use apiDocs JS for our documentation currently.
- Run the following to generate the API documention to be able to know how to use the Chess Game API
  ```
  docker-compose run api npm run api-docs
  ```
- Open the html file at `./apidoc/index.html` in your browser to see api documentation

## Testing
- There is a mixture of `api` and `unit test` cases for the project
- Run the following to run all test cases
  ```
  docker-compose run api npm test
  ```
- Note, in the interest of time, the `api` test cases currently runs against the current mongodb, this would be in its own docker-compose-test.yaml file.

## Closing
- To stop the server, run the following command to stop and remove the docker containers
  ```
  docker-compose down
  ```
