import assert from 'assert'
import supertest from 'supertest'

import app from '../../src/app'

import initialBoardState from '../../src/config/initial-board-state'
import { PlayerColor } from '../../src/lib/piece'


let server = null
describe('Games API', () => {
  before(() => {
    server = app.listen(9001)
  })

  after(() => {
    server.close()
  })

  describe('create game', () => {
    it('should create game with initial state and return 200', async () => {
      const response = await supertest(server)
        .post('/games')
        .send({})
        .expect(200)

      assert.deepStrictEqual(response.body.board, initialBoardState)
      assert.deepStrictEqual(response.body.playersTurn, PlayerColor.white)
    })
  })

  describe('current game state with next player', () => {
    it('should return current game state without any moves yet', async () => {
      const newGameRespnse = await supertest(server)
        .post('/games')
        .send({})
        .expect(200)

      const savedGameResponse = await supertest(server)
        .get(`/games/${newGameRespnse.body._id}`)
        .expect(200)

      assert.deepStrictEqual(savedGameResponse.body.board, initialBoardState)
      assert.strictEqual(savedGameResponse.body.playersTurn, PlayerColor.white)
    })

    it('should return current game state at multiple moves after creation', async () => {
      const newGameRespnse = await supertest(server)
        .post('/games')
        .send({})
        .expect(200)

      const gameId = newGameRespnse.body._id

      await supertest(server)
        .post(`/games/${gameId}/move`)
        .send({
          from: 'a2',
          to: 'a4',
          player: PlayerColor.white
        })
        .expect(200)

      await supertest(server)
        .post(`/games/${gameId}/move`)
        .send({
          from: 'b7',
          to: 'b5',
          player: PlayerColor.black
        })
        .expect(200)

      await supertest(server)
        .post(`/games/${gameId}/move`)
        .send({
          from: 'a4',
          to: 'b5',
          player: PlayerColor.white
        })
        .expect(200)

      const savedGameResponse = await supertest(server)
        .get(`/games/${gameId}`)
        .expect(200)

      // Expected end result of board
      const expectedBoard = JSON.parse(JSON.stringify(initialBoardState))

      const orginalBlack = {row: 1, col: 1}
      const orginalWhite = {row: 6, col: 0}
      const finalSquare = {row: 3, col: 1}

      expectedBoard[finalSquare.row][finalSquare.col] = expectedBoard[orginalWhite.row][orginalWhite.col]
      expectedBoard[finalSquare.row][finalSquare.col].initial = false
      expectedBoard[orginalWhite.row][orginalWhite.col] = null
      expectedBoard[orginalBlack.row][orginalBlack.col] = null


      assert.deepStrictEqual(savedGameResponse.body.board, expectedBoard)
      assert.strictEqual(savedGameResponse.body.playersTurn, PlayerColor.black)
    })

    it('should return not found for unknown game id', async () => {
      await supertest(server)
        .get(`/games/61914b7fe1e145ddddcab910`)
        .expect(404)
    })

    it('should return bad request with invalid id', async () => {
      await supertest(server)
        .get(`/games/123`)
        .expect(400)
    })
  })

  describe('potential moves', () => {
    it('should return potential moves for pawn with attacks', async () => {
      const newGameRespnse = await supertest(server)
        .post('/games')
        .send({})
        .expect(200)

      const gameId = newGameRespnse.body._id

      await supertest(server)
        .post(`/games/${gameId}/move`)
        .send({
          from: 'a2',
          to: 'a4',
          player: PlayerColor.white
        })
        .expect(200)

      await supertest(server)
        .post(`/games/${gameId}/move`)
        .send({
          from: 'b7',
          to: 'b5',
          player: PlayerColor.black
        })
        .expect(200)

      const movesResponse = await supertest(server)
        .get(`/games/${gameId}/potential/a4`)
        .expect(200)

      const expected = [
        { position: 'a5', capture: false },
        { position: 'b5', capture: true }
      ]
      assert.deepStrictEqual(movesResponse.body, expected)
    })

    it('should only allow request for pawn piece', async () => {
      const newGameRespnse = await supertest(server)
        .post('/games')
        .send({})
        .expect(200)

      const gameId = newGameRespnse.body._id

      await supertest(server)
        .get(`/games/${gameId}/potential/a1`)
        .expect(400)
    })
  })

  describe('history', () => {
    it('should return an error for this endpoint as it is not implemented as yet', async () => {
      const newGameRespnse = await supertest(server)
        .post('/games')
        .send({})
        .expect(200)

      const gameId = newGameRespnse.body._id
      await supertest(server)
        .get(`/games/${gameId}/history`)
        .expect(501)
    })
  })
})