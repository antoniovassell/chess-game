import assert from 'assert'
import BadRequestHttpException from '../../../src/common/bad-request-http-error'
import GameService from "../../../src/services/game.service"

describe('GameService', () => {
  describe('convertPositionToNumber', () => {
    it('should convert a8 to row 0, col 0', () => {
      const gameService = new GameService()

      const expected = {
        row: 0,
        col: 0
      }
      assert.deepStrictEqual(gameService.convertPositionToNumber('a8'), expected)
    })

    it('should convert h1 to row 7, col 7', () => {
      const gameService = new GameService()

      const expected = {
        row: 7,
        col: 7
      }
      assert.deepStrictEqual(gameService.convertPositionToNumber('h1'), expected)
    })

    it('should convert d4 to row 4 , col 3', () => {
      const gameService = new GameService()

      const expected = {
        row: 4,
        col: 3
      }
      assert.deepStrictEqual(gameService.convertPositionToNumber('d4'), expected)
    })

    it('should throw an error for invalid position', () => {
      const gameService = new GameService()
      assert.throws(() => {
        gameService.convertPositionToNumber('z3')
      },
        new BadRequestHttpException('InvalidPosition', 'Invalid position')
      )
    })
  })


})