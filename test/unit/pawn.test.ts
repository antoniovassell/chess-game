import assert from "assert"
import Board from "../../src/lib/board"
import BadRequestHttpException from "../../src/common/bad-request-http-error"
import initialBoard from "../../src/config/initial-board-state"
import Pawn from "../../src/lib/pawn"
import { PieceTypes, PlayerColor } from "../../src/lib/piece"

describe('Pawn', () => {
  describe('move', () => {
    context('white player', () => {
      it('should be able to move 2 steps on first move', () => {
        const board = new Board(initialBoard)
        const currentPosition = {row: 6, col: 0}
        const pawn = new Pawn(
          board,
          PlayerColor.white,
          currentPosition,
          true
        )

        const nextPosition = {row: 4, col: 0}

        pawn.move({
          from: currentPosition,
          to: nextPosition
        })

        // assert get position is correct
        assert.deepStrictEqual(pawn.getPosition(), nextPosition)

        // assert board is updated
        const pieceDetail = board.layout[nextPosition.row][nextPosition.col]
        const expected = {
          initial: false,
          pieceType: PieceTypes.pawn,
          color: PlayerColor.white
        }
        assert.deepStrictEqual(pieceDetail, expected)
      })

      it('should not move more than 1 step on second move', () => {
        const board = new Board(initialBoard)
        const currentPosition = {row: 7, col: 0}
        const pawn = new Pawn(
          board,
          PlayerColor.white,
          currentPosition,
          false
        )

        const nextPosition = {row: 3, col: 0}

        assert.throws(() => {
          pawn.move({
            from: currentPosition,
            to: nextPosition
          })
        }, new BadRequestHttpException('InvalidMove', 'Invalid move'))
      })

      it('should not be able to move forward with an obstacle', () => {
        const board = new Board(initialBoard)
        board.layout[5][0] = {
          initial: false,
          color: PlayerColor.black,
          pieceType: PieceTypes.pawn
        }

        const currentPosition = {row: 6, col: 0}
        const pawn = new Pawn(
          board,
          PlayerColor.white,
          currentPosition,
          true
        )

        const nextPosition = {row: 4, col: 0}

        assert.throws(() => {
          pawn.move({
            from: currentPosition,
            to: nextPosition
          })
        }, new BadRequestHttpException('InvalidMove', 'Invalid move'))
      })

      it('should be able to attack other player on left diagonal', () => {
        const board = new Board(initialBoard)
        board.layout[3][0] = {
          initial: false,
          color: PlayerColor.black,
          pieceType: PieceTypes.pawn
        }

        board.layout[4][1] = {
          initial: false,
          color: PlayerColor.white,
          pieceType: PieceTypes.pawn
        }

        const currentPosition = {row: 4, col: 1}
        const pawn = new Pawn(
          board,
          PlayerColor.white,
          currentPosition,
          false
        )

        const nextPosition = {row: 3, col: 0}

        pawn.move({
          from: currentPosition,
          to: nextPosition
        })

        // assert get position is correct
        assert.deepStrictEqual(pawn.getPosition(), nextPosition)

        // assert board is updated
        const pieceDetail = board.layout[nextPosition.row][nextPosition.col]
        const expected = {
          initial: false,
          pieceType: PieceTypes.pawn,
          color: PlayerColor.white
        }
        assert.deepStrictEqual(pieceDetail, expected)
      })

      it('should be able to attack other player on right diagonal', () => {
        const board = new Board(initialBoard)
        // right black piece to attack
        board.layout[3][2] = {
          initial: false,
          color: PlayerColor.black,
          pieceType: PieceTypes.pawn
        }

        // set piece to current position
        const currentPosition = {row: 4, col: 1}
        board.layout[currentPosition.row][currentPosition.col] = {
          initial: false,
          color: PlayerColor.white,
          pieceType: PieceTypes.pawn
        }

        const pawn = new Pawn(
          board,
          PlayerColor.white,
          currentPosition,
          true
        )

        const nextPosition = {row: 3, col: 2}

        pawn.move({
          from: currentPosition,
          to: nextPosition
        })

        // assert get position is correct
        assert.deepStrictEqual(pawn.getPosition(), nextPosition)

        // assert board is updated
        const pieceDetail = board.layout[nextPosition.row][nextPosition.col]
        const expected = {
          initial: false,
          pieceType: PieceTypes.pawn,
          color: PlayerColor.white
        }
        assert.deepStrictEqual(pieceDetail, expected)
      })
    })
  })
})